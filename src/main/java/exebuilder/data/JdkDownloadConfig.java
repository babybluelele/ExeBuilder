package exebuilder.data;

public class JdkDownloadConfig {

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getOutputPath() {
    return outputPath;
  }

  public void setOutputPath(String outputPath) {
    this.outputPath = outputPath;
  }

  private String name;
  private String url;
  private String outputPath;

  public static JdkDownloadConfig jdkDownloadConfig;

  public static JdkDownloadConfig getInstance() {
    if (jdkDownloadConfig == null) {
      jdkDownloadConfig = new JdkDownloadConfig();
    }
    return jdkDownloadConfig;
  }
}
