package exebuilder.core;

import javafx.application.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Launcher {

  private static final Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    logger.debug("应用程序当前路径：{}", System.getProperty("user.dir"));
    Application.launch(AppMain.class, args);
  }

}
